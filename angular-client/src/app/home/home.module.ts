import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ClarityModule } from 'clarity-angular';

//import { routes } from './home.routes';
import { HomeComponent } from './home.component';
import { RecentComponent } from './home-components/recent-component/recent.component';
import { WeatherComponent } from './home-components/weather-component/weather.component';

@NgModule({
  declarations: [
    HomeComponent,
    RecentComponent,
    WeatherComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule.forRoot(),
    //RouterModule.forChild(routes),
  ],
})
export class HomeModule {
  //public static routes = routes;
}