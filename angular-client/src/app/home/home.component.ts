/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component, OnInit } from '@angular/core';
import { RecentComponent } from './home-components/recent-component/recent.component';

@Component({
    styleUrls: ['home.component.scss'],
    templateUrl: 'home.component.html',
})
export class HomeComponent implements OnInit {

    ngOnInit() {

    }
}
