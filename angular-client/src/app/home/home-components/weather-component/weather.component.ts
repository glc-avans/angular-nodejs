import { Component, OnInit } from '@angular/core';
import { Weather } from '../../../_interfaces/weather';
import { OwmService } from '../../../_services/owm.service';

@Component({
    selector: 'com-weather',
    styleUrls: ['weather.component.scss'],
    templateUrl: 'weather.component.html',
    providers: [OwmService],
})
export class WeatherComponent implements OnInit {

    weather: JSON;

    constructor(private owmService: OwmService) {}

    ngOnInit() {
        this.loadWeatherData();
    }

    loadWeatherData() {
        this.owmService.getWeather().subscribe (
            weather => console.log(weather), //Bind to view
            err => {
                // Log errors if any
                console.log(err);
        });
    }
}
