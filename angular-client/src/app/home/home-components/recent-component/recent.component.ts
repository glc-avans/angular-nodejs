import { Component, OnInit } from '@angular/core';
import { Flight } from '../../../_utils_dev/flight';
import { Inventory } from '../../../_utils_dev/inventory';

@Component({
    selector: 'card-recent',
    styleUrls: ['recent.component.scss'],
    templateUrl: 'recent.component.html',
    providers: [Inventory],
})
export class RecentComponent implements OnInit {

    private modalopen: Boolean;

    flights: Flight[];

    constructor(private inventory: Inventory) {
        inventory.size = 103;
        inventory.reset();
        setTimeout(() => {
            this.flights = inventory.all;
        }, 2000);
        
    }

    ngOnInit() {

    }

    public OpenModal() {
        console.log("OpenModal();");
        this.modalopen = true;
    }

    public CloseModal() {
        this.modalopen = false;
    }
}
