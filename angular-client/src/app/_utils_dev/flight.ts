import { User } from "./user";

export interface Flight {
    id: number;
    name: string;
    creation: Date;
    landing: Date;
    user: User;

    // Type for dynamic access to specific properties
    [key: string]: any;
}
