import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'alert-bar',
    styleUrls: ['alert-bar.component.scss'],
    templateUrl: 'alert-bar.component.html',
})
export class AlertComponent implements OnInit {

    connected: Boolean;

    ngOnInit() {
        this.connected = false;
    }
}
