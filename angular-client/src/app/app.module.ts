import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClarityModule } from 'clarity-angular';
import { AppComponent } from './app.component';
import { ROUTING } from './app.routing';
import { HomeModule } from './home/home.module';
import { SettingsModule } from './settings/settings.module';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './root-components/alert-bar/alert-bar.component';
import { HeaderComponent } from './root-components/header/header.component';

@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        HeaderComponent,
        LoginComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        HomeModule,
        SettingsModule,
        ClarityModule.forRoot(),
        ROUTING
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
