import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ClarityModule } from 'clarity-angular';
import { SettingsComponent } from './settings.component';

@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule.forRoot(),
    //RouterModule.forChild(routes),
  ],
})
export class SettingsModule {
  //public static routes = routes;
}