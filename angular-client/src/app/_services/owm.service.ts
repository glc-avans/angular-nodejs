import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class OwmService {

     constructor (private http: Http) {}

     private owmUrl = 'http://api.openweathermap.org/data/2.5/weather?id=2755506&appid=5adfd887d113686651339dc67fd590b0'; 

     // Fetch adata for Gilze Rijen
     getWeather() : Observable<JSON> {

         // ...using get request
         return this.http.get(this.owmUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }
}