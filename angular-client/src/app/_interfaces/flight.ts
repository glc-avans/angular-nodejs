export interface Flight {
    Name: String;
    Plane: string;
    TakeOff: Date;
    Landing: Date;

    // Type for dynamic access to specific properties
    [key: string]: any;
}