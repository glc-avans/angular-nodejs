export class Weather {
    constructor(
        public lon: GLfloat,
        public lan: GLfloat,
        public main: String,
        public description: String,
        public icon: String,
        public temp: GLfloat,
        public pressure: GLint,
        public temp_min: GLfloat,
        public max_temp: GLfloat,
        public visibility: GLint,
        public windspeed: GLfloat,
        public winddirecion: GLint,
    ) { }
}
// {
//    "coord":{
//       "lon":4.95,
//       "lat":51.53
//    },
//    "weather":[
//       {
//          "id":804,
//          "main":"Clouds",
//          "description":"overcast clouds",
//          "icon":"04n"
//       }
//    ],
//    "base":"stations",
//    "main":{
//       "temp":281.37,
//       "pressure":1024,
//       "humidity":75,
//       "temp_min":280.15,
//       "temp_max":283.15
//    },
//    "visibility":10000,
//    "wind":{
//       "speed":3.6,
//       "deg":40
//    },
//    "clouds":{
//       "all":90
//    },
//    "dt":1490558100,
//    "sys":{
//       "type":1,
//       "id":5210,
//       "message":0.0028,
//       "country":"NL",
//       "sunrise":1490506022,
//       "sunset":1490551503
//    },
//    "id":2755506,
//    "name":"Gemeente Gilze en Rijen",
//    "cod":200
// }