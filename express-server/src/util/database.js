import mysql from 'mysql';

import databaseConfig from '../../config/database.json';

const pool  = mysql.createPool(databaseConfig);

export default {
    execute (query) {
        return new Promise((resolve, reject) => {
            pool.getConnection(function(err, connection) {
                if (err) {
                    reject(error);
                }
                // Use the connection
                connection.query(query, function (error, results, fields) {
                    // And done with the connection.
                    connection.release();

                    // Handle error after the release.
                    if (error) {
                        reject(error);
                    }

                    // Don't use the connection here, it has been returned to the pool.
                    resolve({results, fields});
                });
            });
        });
    },

    format (query, data) {
        return mysql.format(query, data);
    }
};
