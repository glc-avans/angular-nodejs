import express      from 'express';
import jwt          from 'jwt-simple';

import db from '../util/database.js';

const router = express.Router();

router.get('/', (req, res) => {
    res.status(200);
    res.json({"hi": "there"});
});

router.get('/users/:id?', (req, res) => {
    const id = req.params.id || null;
    if (id) {
        // id specified.
        db.execute(db.format('SELECT NAME FROM user WHERE CARD_ID = ?', [id]))
        .then((response) => {
            console.log(response);
            res.status(200);
            res.json({results: response.results});
        })
        .catch((error) => {
            console.log(error);
        });
    } else {
        // no id specified.
        console.log('No ID specified');
        db.execute(db.format('SELECT * FROM user;'))
        .then((response) => {
            console.log(response);
            res.status(200);
            res.json({results: response.results});
        })
        .catch((error) => {
            console.log(error);
        });
    }
});

router.post('/user', (req, res) => {
    // var sql         = 'INSERT INTO user (CARD_ID, NAME, ROLE) VALUES (?, ?, ?)';
    // const values    = []
    db.execute()
});

export default router;
