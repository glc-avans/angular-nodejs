#defining variables for RPI
ARM_DATABASE="hypriot/rpi-mysql:5.5"
ARM_NODE="hypriot/rpi-node:7.4.0"

#defining variables for X86 X64
X86_64_DATABASE="mysql:5.7.18"
X86_64_NODE="node:7.4.0"

#clear the terminal window
clear

echo "This will install all necesary dependencies and packages to run this project"

#detect system architecture
echo "Detecting system architecture"
ARCH=`uname -m`
echo "Detected $ARCH"

echo "Installing Docker"
#curl -sSL https://get.docker.com | sh
apt-get update
apt-get install python-pip -y
pip install docker-compose --upgrade

echo "Generating config files"
#Setup Dockerfile for ARM
if [[ $ARCH == arm* ]] ;
then
cp ./config/Dockerfile_EMPTY_angular angular-client/Dockerfile
sed -i 's,'"____NODE_IMAGE____"', '$ARM_NODE',' "angular-client/Dockerfile"

cp ./config/Dockerfile_EMPTY_api express-server/Dockerfile
sed -i 's,'"____NODE_IMAGE____"', '$ARM_NODE',' "express-server/Dockerfile"

cp ./config/docker-compose_EMPTY.yml ./docker-compose.yml
sed -i 's,'"____MYSQL_IMAGE____"', '$ARM_DATABASE',' "./docker-compose.yml"

fi

#Setup Dockerfiles for X86_64
if [[ $ARCH == "x86_64" ]] ;
then
cp ./config/Dockerfile_EMPTY_angular angular-client/Dockerfile
sed -i 's,'"____NODE_IMAGE____"', '$X86_64_NODE',' "angular-client/Dockerfile"

cp ./config/Dockerfile_EMPTY_api express-server/Dockerfile
sed -i 's,'"____NODE_IMAGE____"', '$X86_64_NODE',' "express-server/Dockerfile"

cp ./config/docker-compose_EMPTY.yml ./docker-compose.yml
sed -i 's,'"____MYSQL_IMAGE____"', '$X86_64_DATABASE',' "./docker-compose.yml"

cat config/phpmyadmin.config >> ./docker-compose.yml
fi

echo "extracting database volume"
if [ -d "./database" ]; then
 echo "Database directory already exists... skipping extraction"
else
 tar xzf ./config/database.tar.gz
fi

echo "Creating cronjob"
crontab -l > ./config/cron
echo "@reboot docker-compose up -d" >> ./config/cron
crontab ./config/cron

echo "Installation completed succesfully!"
echo "Reboot for all changes to take effect"
if read -r -s -n 1 -t 5 -p "Rebooting press any ket to abort:" key #key in a sense has no use at all
then
 echo "Reboot for all changes to take effect"
else
 reboot
fi
exit 0
