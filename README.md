# MEAN #

### MEAN angular/nodejs application ###

* This repository is configured to be used with docker please take note of this.
* Version 0.1

### How do I get set up for production? ###

* run SETUP_PROD.sh (following soon)
* If all went well logout and log back in. Run $ docker-compose build && docker-compose up -d
* After completion (+- 5 minutes) all is setup and running

* localhost:80   > angular app
* localhost:3000 > nodejs server
* localhost:6969 > PHPMYADMIN
* localhost:3306 > mysql


### How do I get set up for Development? ###

* run SETUP_DEV.sh (following soon)
* To start angular run $ npm start .
* To start nodejs run $ npm start . 
* After completion (+- 2 minutes) all is setup and running

* localhost:80   > angular app
* localhost:3000 > nodejs server
* localhost:6969 > PHPMYADMIN
* localhost:3306 > mysql

### Contact ###

* Frank van Veen (Scriptdwarf)